package net.sytes.rokkosan.myfileselectiondialog

// 2022 Mar. 21.
// 2022 Mar. 19.
// Ryuichi Hashimoto.
/*
 * This is java-to-kotlin-conversion of the java code in
 * https://www.hiramine.com/programming/android/fileselectiondialog.html
 */

/*
 * Sample application of "displaying ListView in dialog" and
 * "sending a selected item on ListView to MainActivity"
 */

import android.Manifest
import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Environment
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import net.sytes.rokkosan.myfileselectiondialog.databinding.ActivityMainBinding
import java.io.File

class MainActivity : AppCompatActivity(), FileSelectionDialog.OnFileSelectListener {

    // 定数
    private val MENUID_FILE = 0 // ファイルメニューID

    // メンバー変数
    private var mStrInitialDir: String = Environment.getExternalStorageDirectory().path // 初期フォルダ
    private val binding: ActivityMainBinding by lazy { ActivityMainBinding.inflate(layoutInflater) }
    private val mainLayout: View by lazy { binding.root }
    private val myPermission = Manifest.permission.READ_EXTERNAL_STORAGE
    private var isCheckedNotAskAgain: Boolean = false
    private val sharedPref: SharedPreferences by lazy {
        this.getSharedPreferences("myPref", Context.MODE_PRIVATE) }

    // registerForActivityResult()を定義してlaunch()を呼び出すと、
    // パーミッションが許可されていない時はユーザーにパーミッション許可を促すダイアログが表示される。
    // パーミッションが「許可された時」「許可されなかった時」のコールバック関数を記述する
    private val launcher = registerForActivityResult(
            ActivityResultContracts.RequestPermission())
        { granted ->
        if (granted) {
            // パーミッションが取得されている時
            // パーミッション取得後にすべき処理に移る
            isCheckedNotAskAgain = false
            with (sharedPref.edit()) {
                putBoolean("isCheckedNotAskAgain", isCheckedNotAskAgain)
                apply()
            }
            myWorkWithPermission()
        } else {
            if (shouldShowRequestPermissionRationale(myPermission)) {
                // パーミッション取得を拒否され、
                // 「今後表示しない」は選択されていない
                isCheckedNotAskAgain = false
                with (sharedPref.edit()) {
                    putBoolean("isCheckedNotAskAgain", isCheckedNotAskAgain)
                    apply()
                }
            } else {
                // パーミッション取得を拒否され、
                // 「今後表示しない」が選択されて再リクエストも拒否されている
                isCheckedNotAskAgain = true
                with (sharedPref.edit()) {
                    putBoolean("isCheckedNotAskAgain", isCheckedNotAskAgain)
                    apply()
                }
            }
            myWorkWithoutPermission()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // setContentView(R.layout.activity_main)
        setContentView(binding.root)

        isCheckedNotAskAgain = sharedPref.getBoolean("isCheckedNotAskAgain", false)
        getMyPermission()
    }

    // パーミッションを取得するメソッド
    private fun getMyPermission() {
        if ( ContextCompat.checkSelfPermission(this, myPermission) ==
            PackageManager.PERMISSION_GRANTED) {
            // 既にパーミッションが取得されている時
            // パーミッション取得後にすべき処理に移る
            isCheckedNotAskAgain = false
            with (sharedPref.edit()) {
                putBoolean("isCheckedNotAskAgain", isCheckedNotAskAgain)
                apply()
            }
            myWorkWithPermission()
        } else {
            // パーミッション未取得
            if (isCheckedNotAskAgain) {
                // 過去に権限取得を拒否され、「今後表示しない」が選択されて再リクエストも拒否されている
                // ダイアログでユーザーに説明し、ユーザーが望めば設定画面を表示する
                val openConfigDialogFragment = OpenConfigDialogFragment()
                openConfigDialogFragment.show(supportFragmentManager, "simple")

                // ダイアログ後の処理
                if ( ContextCompat.checkSelfPermission(this, myPermission) ==
                    PackageManager.PERMISSION_GRANTED) {
                    // パーミッション取得している
                    isCheckedNotAskAgain = false
                    with (sharedPref.edit()) {
                        putBoolean("isCheckedNotAskAgain", isCheckedNotAskAgain)
                        apply()
                    }
                    myWorkWithPermission()
                } else {
                    // パーミッション未取得
                    if (shouldShowRequestPermissionRationale(myPermission)) {
                        // 「今後表示しない」は選択されていない
                        isCheckedNotAskAgain = false
                        with (sharedPref.edit()) {
                            putBoolean("isCheckedNotAskAgain", isCheckedNotAskAgain)
                            apply()
                        }
                    } else {
                        // 「今後表示しない」が選択されている
                        isCheckedNotAskAgain = true
                        with (sharedPref.edit()) {
                            putBoolean("isCheckedNotAskAgain", isCheckedNotAskAgain)
                            apply()
                        }
                    }
                    myWorkWithoutPermission()
                }
            } else {
                // パーミッションを取得しておらず、
                // 過去に権限取得で再リクエストの拒否はしていない。
                launcher.launch(myPermission)
            }
        }
    }

    fun myWorkWithoutPermission() {
        Snackbar.make(
            binding.root, getText(R.string.failed_permission), Snackbar.LENGTH_LONG)
            .show()
    }

    fun myWorkWithPermission() {
        Snackbar.make(
            binding.root, getText(R.string.got_permission), Snackbar.LENGTH_SHORT)
            .show()
    }

    // オプションメニュー生成時
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        super.onCreateOptionsMenu(menu)
        menu.add(0, MENUID_FILE, 0, "Select File...")
        return true
    }

    // オプションメニュー選択時
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            MENUID_FILE -> {
                // ダイアログオブジェクトを生成し表示する
                val dlg = FileSelectionDialog(this, this)
                dlg.show(File(mStrInitialDir))
                return true
            }
        }
        return false
    }

    // ファイルが選択されたときに呼び出される関数
    override fun onFileSelect(file: File) {
        binding.textViewInfo.text = getString(R.string.selected_path, file.getPath())
        mStrInitialDir = file.getParent()!!
    }
}
