package net.sytes.rokkosan.myfileselectiondialog

// 2022 Mar. 21.
// 2022 Mar. 19.
// Ryuichi Hashimoto.
/*
 * This is java-to-kotlin-conversion of the java code in
 * https://www.hiramine.com/programming/android/fileselectiondialog.html
 */

import android.app.AlertDialog
import android.content.Context
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.*
import java.io.File
import java.util.*

class FileSelectionDialog(val m_contextParent: Context, val m_listener: OnFileSelectListener)
        : AdapterView.OnItemClickListener {

    // 選択したファイルの情報を取り出すためのリスナーインターフェース
    interface OnFileSelectListener {
        // ファイルが選択されたときに呼び出される関数
        fun onFileSelect(file: File)
    }

    private lateinit var m_dialog: AlertDialog
    private lateinit var mFileInfoArrayAdapter:  FileInfoArrayAdapter

    // ダイアログの作成と表示
    fun show(fileDirectory: File) {
        // タイトル
        val strTitle = fileDirectory.absolutePath

        // リストビュー
        val listview = ListView(m_contextParent)
        listview.setScrollingCacheEnabled(false)
        listview.setOnItemClickListener(this)
        // ファイルリスト
        val aFile = fileDirectory.listFiles()
        val listFileInfo: MutableList<FileInfo> = ArrayList()
        if (null != aFile) {
            for (fileTemp in aFile) {
                listFileInfo.add(FileInfo(fileTemp.name, fileTemp))
            }
            Collections.sort(listFileInfo)
        }
        // 親フォルダに戻るパスの追加
        if (null != fileDirectory.parent) {
            listFileInfo.add(0, FileInfo("..", File(fileDirectory.parent!!)))
        }
        mFileInfoArrayAdapter = FileInfoArrayAdapter(m_contextParent, listFileInfo)
        listview.setAdapter(mFileInfoArrayAdapter)
        val builder: AlertDialog.Builder = AlertDialog.Builder(m_contextParent)
        builder.setTitle(strTitle)
        builder.setNegativeButton("Cancel", null)
        builder.setView(listview)
        m_dialog = builder.show()
    }

    // ListView内の項目をクリックしたときの処理
    override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        m_dialog.dismiss()
        val mFileInfo = mFileInfoArrayAdapter.getItem(position)
        if (mFileInfo.getFile().isDirectory) {
            show(mFileInfo.getFile())
        } else {
            // ファイルが選ばれた：リスナーのハンドラを呼び出す
            m_listener.onFileSelect(mFileInfo.getFile())
        }
    }

    class FileInfo(val m_strName: String, val m_file: File): Comparable<FileInfo> {
        fun getName(): String {
            return m_strName
        }

        fun getFile(): File {
            return m_file
        }

        // 比較
        override operator fun compareTo(other: FileInfo): Int {
            // ディレクトリ < ファイル の順
            if (m_file.isDirectory() && !other.getFile().isDirectory()) {
                return -1
            }
            return if (!m_file.isDirectory() && other.getFile().isDirectory()) {
                1
            } else m_file.getName().lowercase(Locale.getDefault())
                .compareTo(other.getFile().getName().lowercase(Locale.getDefault()))
            // ファイル同士、ディレクトリ同士の場合は、
            // ファイル名（ディレクトリ名）の大文字小文字区別しない辞書順
        }
    }

    class FileInfoArrayAdapter(var m_context: Context, var m_listFileInfo: List<FileInfo>)
            : BaseAdapter() {
        override fun getCount(): Int {
            return m_listFileInfo.size
        }

        override fun getItem(position: Int): FileInfo {
            return m_listFileInfo[position]
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        internal class ViewHolder {
            var textviewFileName: TextView? = null
            var textviewFileSize: TextView? = null
        }

        // 一要素のビューの生成
        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            var mConvertView = convertView

            // var convertView: View? = convertView
            val viewHolder: ViewHolder
            if (null == mConvertView) {
                // レイアウト
                val layout = LinearLayout(m_context)
                layout.orientation = LinearLayout.VERTICAL
                layout.layoutParams =
                    LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                    )
                // ファイル名テキスト
                val textviewFileName = TextView(m_context)
                textviewFileName.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 24f)
                layout.addView(
                    textviewFileName,
                    LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                    )
                )
                // ファイルサイズテキスト
                val textviewFileSize = TextView(m_context)
                textviewFileSize.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12f)
                layout.addView(
                    textviewFileSize,
                    LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                    )
                )
                mConvertView = layout
                viewHolder = ViewHolder()
                viewHolder.textviewFileName = textviewFileName
                viewHolder.textviewFileSize = textviewFileSize
                mConvertView.setTag(viewHolder)
            } else {
                viewHolder = mConvertView.getTag() as ViewHolder
            }
            val mFileInfo = m_listFileInfo[position]
            if (mFileInfo.getFile().isDirectory) {
                // ディレクトリの場合は、名前の後ろに「/」を付ける
                viewHolder.textviewFileName!!.text = m_context.getString(R.string.addSlash, mFileInfo.getName())
                viewHolder.textviewFileSize!!.text = m_context.getString(R.string.directory)
            } else {
                viewHolder.textviewFileName!!.text = mFileInfo.getName()
                viewHolder.textviewFileSize!!.text =
                    m_context.getString(R.string.fileSize,(mFileInfo.getFile().length() / 1024).toString())
            }
            return mConvertView
        }
    }
}